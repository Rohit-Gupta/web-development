<?php

  include ("../functions.php");
  echo "Hello World";
  spaces(1,"",0);
  echo "This spans
  multiple lines. The newlines will be
  output as well";
  spaces(1,"",0);
  echo "This spans\nmultiple lines. The newlines will be\noutput as well.";
  spaces(1,"",0);
  echo "Escaping characters is done \"Like this\".";
  spaces(1,"",0);
  // You can use variables inside of an echo statement
  $foo = "foobar";
  $bar = "barbaz";

  echo "foo is $foo"; // foo is foobar
  spaces(1,"",0);
  // You can also use arrays
  $baz = array("value" => "foo");

  echo "this is {$baz['value']} !"; // this is foo !
  spaces(1,"",0);
  // Using single quotes will print the variable name, not the value
  echo 'foo is $foo'; // foo is $foo
  spaces(1,"",0);
  // If you are not using any other characters, you can just echo variables
  echo $foo;          // foobar
  spaces(1,"",0);
  echo $foo,$bar;     // foobarbarbaz
  spaces(1,"",0);
  // Strings can either be passed individually as multiple arguments or
  // concatenated together and passed as a single argument
  echo 'This ', 'string ', 'was ', 'made ', 'with multiple parameters.', chr(10);
  echo 'This ' . 'string ' . 'was ' . 'made ' . 'with concatenation.' . "\n";
  spaces(1,"",0);
  echo "<input type='text' value='Rohit Gupta'/>";
  spaces(10,"",0);
  echo "<h2>Rohit Gupta</h2>";

 ?>
