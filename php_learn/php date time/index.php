<?php

	//to show date
	echo date('d/m/y')."<br>";
	echo date('D/M/Y')."<br>";
	echo date('d-m-y')."<br>";
	echo date('D-M-Y')."<br>";

	//to show time
	echo date('h:i:s')."<br>";

	//to set time zone
	date_default_timezone_set("Asia/Kolkata");
	echo date("D-M-Y h:i:s")."<br>";

	//to print time with AM/PM
	echo date('h:i:s A')."<br>";

	//to ptint 24 hours format
	echo date('H:i:s')."<br>";

 ?>
