<?php

	  //solution to single qoute problem in echo is as follows
	  $name='Rohit Gupta' ;
	  echo 'name '.$name.'<br>';

	  //prints the string length of "hello world"
	  echo strlen("Hello world!")."<br";

	  //counts the number of words in a string
	  echo str_word_count("Hello world!")."<br";

	  //to reverse a string
	  echo strrev("Hello world!")."<br";

	  //searches for a specific word in a string
	  echo strpos("Hello world!", "world")."<br";

	  //to replace text within a string
	  echo str_replace("world", "Dolly", "Hello world!")."<br";

	  //to convert to upper
	  echo strtoupper("string")."<br"

	  //to convert to upper
	  echo strtolower("STRING")."<br";

	  //to get a substring
	  echo substr($name,2)."<br>";
	  echo substr($name,2,5)."<br>";

	  //to suffle elements of string
	  echo str_shuffle($name);
 ?>
