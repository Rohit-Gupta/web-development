<?php

  function name()
  {
    echo "my name is Rohit Gupta<br>";
  }

  name();

  function familyName($fname)
  {
    echo "$fname Refsnes.<br>";
  }

  familyName("Jani");
  familyName("Hege");
  familyName("Stale");

  function setHeight($minheight = 50)
  {
    echo "The height is : $minheight <br>";
  }

  setHeight(350);
  setHeight(); // will use the default value of 50

  function sum($x, $y)
  {
    $z = $x + $y;
    return $z;
  }

  echo "5 + 10 = " . sum(5, 10) . "<br>";
  echo "7 + 13 = " . sum(7, 13) . "<br>";

  $val=sum(2, 4);
  echo "2 + 4 = " .$val ;
 ?>
