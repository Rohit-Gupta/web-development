/*
PHP $_GET can also be used to collect form data after submitting an HTML form with method="get".
$_GET can also collect data sent in the URL.

*/
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
<body>

  <?php
  echo "Study " . $_GET['subject'] . " at " . $_GET['web'];
  ?>

</body>
</html>
