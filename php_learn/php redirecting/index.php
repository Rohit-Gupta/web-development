<?php

	//to redirect to a certain page
	header("Location: http://iic.du.ac.in/");

	echo "<p>Redirecting.......</p>";

	//to redirect after a given number of seconds
	header("refresh: 5; url=http://du.ac.in/du/"); //5 seconds

	//to redirect using javascript
	echo "<script>
	window.location.assign('http://du.ac.in/du/')
	</script>";

	//to redirect using javascript with alert message
	echo "
	<script>
	alert('Redirecting');
	window.location.assign('http://du.ac.in/du/')
	</script>";
 ?>
