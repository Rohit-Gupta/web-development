<?php


  //1. String
  $val="Rohit Gupta";

  //2. Integer
  $val=34;

  //3. float
  $val=34.56;

  //4. bolean
  $val=true;

  //5. null
  $val=null;
  //or
  $val="";

  //6. array
  $val=array("Volvo", "BMW", "Toyota");

  //7. Object
  class Car {
    function Car() {
        $this->model = "VW";
    }
  }

  // create an object
  $herbie = new Car();
  echo $herbie->model;

 ?>
