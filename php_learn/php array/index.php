<?php

	include("../functions.php");
//arrays are created with the help of $arrayName = array('' => , );
  $cars = array("Volvo", "BMW", "Toyota");
  echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . "."."<br>";

  $cars = ["Volvo", "BMW", "Toyota"];
//to print the length of an array
  spaces(1,"to print the length of an array",1);
  echo count($cars)."<br>";

//to prit all elements of array
  spaces(1,"to prit all elements of array",1);
  for($x = 0; $x < count($cars); $x++)
  {
    echo $cars[$x];
    spaces(1,"",0);
  }
//or
//print_r is suitable for associative arrays, as it print values along with values
  spaces(1,"",0);
  print_r($cars);

//php associated array
  spaces(2,"",0);
  echo "php associated array<br>";
  $numbers = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
  echo "Peter is " . $numbers['Peter'] . " years old.";
//or
  spaces(1,"",0);
  foreach($numbers as $x => $x_value)
  {
    echo "Key=" . $x . ", Value=" . $x_value;
    spaces(1,"",0);
  }
  spaces(1,"",0);
//or
  print_r($numbers);
/*

  sort() - sort arrays in ascending order
  rsort() - sort arrays in descending order
  asort() - sort associative arrays in ascending order, according to the value
  ksort() - sort associative arrays in ascending order, according to the key
  arsort() - sort associative arrays in descending order, according to the value
  krsort() - sort associative arrays in descending order, according to the key

*/

  $numbers = array(4, 6, 2, 22, 11);
  sort($numbers);
  spaces(2,"sort",1);
  for($x = 0; $x < count($numbers); $x++)
  {
    echo $numbers[$x];
    echo " ";
  }
  spaces(1,"",0);

  $numbers = array(4, 6, 2, 22, 11);
  rsort($numbers);
  spaces(1,"rsort",1);
  for($x = 0; $x < count($numbers); $x++)
  {
    echo $numbers[$x];
    echo " ";
  }
  spaces(1,"",0);

  $numbers = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
  asort($numbers);
  spaces(1,"asort",1);
  print_r($numbers);

  $numbers = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
  ksort($numbers);
  spaces(2,"ksort",1);
  print_r($numbers);

  $numbers = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
  arsort($numbers);
  spaces(2,"asort",1);
  print_r($numbers);

  $numbers = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
  krsort($numbers);
  spaces(2,"krsort",1);
  print_r($numbers);

 ?>
